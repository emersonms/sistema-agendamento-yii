<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
<div class="row">
    <div class="col-sm-3">
        <h4>Alterar Sala</h4>
    </div>
</div>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-12\">{input}</div>\n<div class=\"col-sm-12\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-1 control-label'],
    ],
]); ?>
<div class="row">
    <div class="col-sm-3">
        <form id="login-form" method="post" role="form">
            <?= $form->field($model, 'nome', [
                'inputOptions' => [
                    'class' => 'form-control',
                    'tabindex' => '1',
                    'placeholder' => 'Nome',
                    'enableClientValidation' => false
                ]])->textInput(['class'=>'form-control']); ?>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <?= Html::submitButton('Alterar', ['class' => 'form-control btn btn-primary', 'name' => 'login-button']); ?>
                    </div>
                </div>
            </div>
        </form>
   </div>
</div>
<?php ActiveForm::end(); ?>