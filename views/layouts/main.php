<?php
use app\widgets\Alert;
use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<?php $this->title = 'Sistema de Agendamento'; ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php $controller = Yii::$app->controller->id; ?>
        <?php $action = Yii::$app->controller->action->id ?>
        <?php if ($controller.'/'.$action !== 'site/login') { ?>
            <div id="throbber" class="throbber-fix"></div>
            <div id="noty-holder"></div>
            <div id="wrapper">
                <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div>teste</div>
                    </div>
                    <ul class="nav navbar-right top-nav margin-right-20">
                        <li class="dropdown">
                            <div class="btn-logout">
                            <?= Html::a('Logout', ['site/logout'], ['class' => 'btn btn-danger']); ?>
                            </div>
                        </li>
                    </ul>
                    <ul class="nav navbar-right top-nav">
                        <li class="dropdown">
                            <div class="bemvindo-usuario">
                                <span class="glyphicon glyphicon-user"></span>
                                <span> Olá <?= Yii::$app->user->identity->nome; ?></span>
                            </div>
                        </li>
                    </ul>
                    <ul class="nav navbar-left top-nav">
                        <li class="dropdown">
                            <div class="titulo-header">Sistema de Agendamento</div>
                        </li>
                    </ul>
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav side-nav">
                            <li>
                                <?= Html::a('Salas', ['site/index']); ?>
                            </li>
                            <?php if ((!Yii::$app->user->isGuest) && (Yii::$app->user->identity->perfil_id == 1)) { ?>
                                <li>
                                    <?= Html::a('Gerenciar Salas', ['sala/listar']); ?>
                                </li>
                                <li>
                                    <?= Html::a('Gerenciar Usuários', ['usuario/listar']); ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </nav>
                <div id="page-wrapper">
                    <div class="container-fluid">
                        <div class="row" id="main" >
                            <div class="col-sm-12 col-md-12" id="content">
                                <?= Alert::widget() ?>
                                <?= $content ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <div class="container"> 
                <?= Alert::widget() ?> 
                <?= $content ?> 
            </div>
        <?php } ?>
    </div>
    <footer class="footer">
        <div class="container"></div>
    </footer>
    <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>