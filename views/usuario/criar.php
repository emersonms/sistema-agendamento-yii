<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
<div class="row">
    <div class="col-sm-3">
        <h4>Criar Usuário</h4>
    </div>
</div>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-12\">{input}</div>\n<div class=\"col-sm-12\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-1 control-label'],
    ],
]); ?>
<div class="row">
    <div class="col-sm-3">
        <form id="login-form" method="post" role="form">
                <?= $form->field($model, 'nome', [
                    'inputOptions' => [
                        'class' => 'form-control',
                        'tabindex' => '1',
                        'placeholder' => 'Nome',
                        'enableClientValidation' => false
                    ]])->textInput(['class'=>'form-control']); ?>
                <?= $form->field($model, 'username', [
                    'inputOptions' => [
                        'class' => 'form-control',
                        'tabindex' => '2',
                        'placeholder' => 'Usuário',
                        'enableClientValidation' => false
                    ]])->textInput(['class'=>'form-control']); ?>
                <?= $form->field($model, 'password', [
                    'inputOptions' => [
                        'class' => 'form-control',
                        'tabindex' => '3',
                        'placeholder' => 'Senha',
                        'enableClientValidation' => false
                    ]])->textInput(['class'=>'form-control']); ?>
                <?= $form->field($model, 'perfil_id')->dropdownList([
                        1 => 'Administrador', 
                        2 => 'Usuário'
                    ],
                    ['prompt'=>'Selecione o Perfil']
                    ); ?>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <?= Html::submitButton('Criar', ['class' => 'form-control btn btn-primary', 'name' => 'login-button']); ?>
                    </div>
                </div>
            </div>
        </form>
   </div>
</div>
<?php ActiveForm::end(); ?>