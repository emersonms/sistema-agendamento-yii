<?php
use yii\grid\GridView;
use yii\helpers\Html;
?>
<div class="row">
    <div class="col-sm-12">
        <div class="titulo-pagina">Gerenciar Usuários</div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Adicionar', ['criar'], ['class' => 'btn btn-primary']); ?>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-sm-8">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'summary'=>"",
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['class' => 'text-center'],
                    'value' => function ($model) {
                        return $model->id;
                    }
                ],
                'nome',
                'username',
                [
                    'attribute' => 'data_create',
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['class' => 'text-center'],
                    'value' => function ($model) {
                        list($data, $hora) = explode(' ', $model->data_create);
                        $data = implode('/', array_reverse(explode('-', $data)));
                        return $data.' '.$hora;
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['class' => 'text-center', 'style' => 'width:30%'],
                    'template' => '{alterar} {deletar}',
                    'buttons' => [
                        'alterar' => function($url, $model, $key) {
                            return Html::a('Alterar', ['alterar', 'id' => $model->id], ['class' => 'btn btn-primary']);
                        },
                        'deletar' => function($url, $model, $key) {
                            return Html::a('Deletar', ['deletar', 'id' => $model->id], [
                                'class' => 'btn  btn-danger',
                                'data-confirm' => "Deseja deletar o usuário '".$model->nome."'?",
                            ]);
                        }
                    ]
                ]
            ],
        ]) ?>
    </div>
</div>