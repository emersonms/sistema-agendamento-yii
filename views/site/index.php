<?php
use yii\helpers\Html;

$this->title = 'Sistema de Agendamento';
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="titulo-pagina">Salas</div>
            </div>
        </div>
        <hr>
        <div class="row">
            <?php foreach ($salas as $sala) { ?>
                <div class="col-sm-4">
                    <ul class="list-group">
                        <li class="list-group-item active">
                            <span><?= $sala->nome; ?></span>
                            <span class="float-right"><?= date('d/m/Y'); ?></span>
                        </li>
                        <li class="list-group-item active font-12">
                            <div class="row">
                                <div class="col-sm-3"><span>Horário</span></div>
                                <div class="col-sm-3 text-center"><div class="agendamento-campos">Status</div></div>
                                <div class="col-sm-3 text-center">Agendado Por</div>
                                <div class="col-sm-3 text-center">Ação</div>
                            </div>
                        </li>
                        <?php foreach ($sala->listaHorariosDia as $horario) {?>
                            <li class="list-group-item <?= ($horario->status == 'Agendado')?'list-group-item-info':''; ?> font-12">
                                <div class="row">
                                    <div class="col-sm-3"><div class="agendamento-campos"><?= $horario->horaInicioFim ?></div></div>
                                    <div class="col-sm-3 text-center"><div class="agendamento-campos"><?= $horario->status; ?></div></div>
                                    <div class="col-sm-3 text-center"><?= $horario->agendadorNome; ?></div>
                                    <div class="col-sm-3 text-center">
                                        <?php if ($horario->status == 'Disponível') { ?>
                                            <?= Html::a('Agendar', ['sala/agendar', 'id' => $horario->id], [
                                                'class' => 'btn btn-primary btn-xs',
                                                'data-confirm' => "Reservar a sala '".$sala->nome."' no horário '".$horario->horaInicioFim."'?",
                                            ]); ?>
                                        <?php } elseif ($horario->status == 'Agendado') { ?>
                                            <?php $reserva_id = $horario->usuarioReservaId; ?>
                                            <?= Html::a('Cancelar', ['sala/cancelar', 'id' => $reserva_id], [
                                                'class' => 'btn btn-danger btn-xs',
                                                'data-confirm' => "Cancelar a reserva da sala '".$sala->nome."' no horário '".$horario->horaInicioFim."'?",
                                            ]); ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
        </div>
    </div>
</div>