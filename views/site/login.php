<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Sistema de Agendamento';
?>
<div class="container">
    <div class="row">
        <div class="col-md-3 col-md-offset-4">
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <h4 class="active2">Sistema de Agendamento</h4>
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'layout' => 'horizontal',
                        'fieldConfig' => [
                            'template' => "{label}\n<div class=\"col-sm-12\">{input}</div>",
                            'labelOptions' => ['class' => 'col-lg-1 control-label'],
                        ],
                    ]); ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <form id="login-form" method="post" role="form" style="display: block;">
                                <div class="form-group">
                                    <?= $form->field($model, 'username', [
                                        'inputOptions' => [
                                            'class' => 'form-control',
                                            'tabindex' => '1',
                                            'placeholder' => 'Usuário',
                                            'enableClientValidation' => false
                                        ]])->textInput(['class'=>'form-control'])->label(false); ?>
                                </div>
                                <div class="form-group">
                                    <?= $form->field($model, 'password', [
                                        'inputOptions' => [
                                            'class' => 'form-control',
                                            'tabindex' => '2',
                                            'placeholder' => 'Senha',
                                            'enableClientValidation' => false
                                        ]])->passwordInput()->label(false); ?>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <?= Html::submitButton('Entrar', ['class' => 'form-control btn btn-primary', 'name' => 'login-button']); ?>
                                        </div>
                                    </div>
                                </div>
                            </form>
                       </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>