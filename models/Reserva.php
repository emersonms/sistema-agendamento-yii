<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reserva".
 *
 * @property string $id
 * @property string $sala_id
 * @property string $usuario_id
 * @property string $sala_horario_id
 *
 * @property SalaHorario $salaHorario
 * @property Sala $sala
 * @property Usuario $usuario
 */
class Reserva extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reserva';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sala_id', 'usuario_id', 'sala_horario_id'], 'required'],
            [['sala_id', 'usuario_id', 'sala_horario_id'], 'integer'],
            [['sala_horario_id'], 'exist', 'skipOnError' => true, 'targetClass' => SalaHorario::className(), 'targetAttribute' => ['sala_horario_id' => 'id']],
            [['sala_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sala::className(), 'targetAttribute' => ['sala_id' => 'id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sala_id' => 'Sala ID',
            'usuario_id' => 'Usuario ID',
            'sala_horario_id' => 'Sala Horario ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalaHorario()
    {
        return $this->hasOne(SalaHorario::className(), ['id' => 'sala_horario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSala()
    {
        return $this->hasOne(Sala::className(), ['id' => 'sala_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }
}
