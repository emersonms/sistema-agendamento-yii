<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property string $id
 * @property string $nome
 * @property string $perfil_id
 * @property string $username
 * @property string $password
 * @property string $authkey
 * @property string $data_create
 * @property integer $ativo
 *
 * @property Fila[] $filas
 * @property Perfil $perfil
 */
class Usuario extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome', 'perfil_id', 'username'], 'required', 'message' => '{attribute} é obrigatório'],
            [['username'], 'unique', 'message' => '{attribute} já existe'],
            [['perfil_id', 'ativo'], 'integer'],
            [['data_create', 'authkey'], 'safe'],
            [['nome'], 'string', 'max' => 255],
            [['username', 'password'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'perfil_id' => 'Perfil',
            'username' => 'Usuário',
            'password' => 'Senha',
            'data_create' => 'Data de Cadastro',
            'ativo' => 'Ativo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerfil()
    {
        return $this->hasOne(Perfil::className(), ['id' => 'perfil_id']);
    }
    
    public function validatePassword($password)
    {
        return $this->password === md5($password);
    }
    
    public static function findIdentity($id)
    {
        $usuario = Usuario::findOne(['id' => $id]);
        return $usuario;
    }
    
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }
        
        return null;
    }
    
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }
        
        return null;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getAuthKey()
    {
        return $this->authkey;
    }
    
    public function validateAuthKey($authKey)
    {
        return $this->authkey === $authKey;
    }
}
