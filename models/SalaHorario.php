<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sala_horario".
 *
 * @property string $id
 * @property string $sala_id
 * @property string $inicio
 * @property string $fim
 * @property string $data
 * @property integer $ativo
 *
 * @property Sala $sala
 */
class SalaHorario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sala_horario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sala_id', 'ativo'], 'integer'],
            [['inicio', 'fim', 'data'], 'safe'],
            [['sala_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sala::className(), 'targetAttribute' => ['sala_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sala_id' => 'Sala',
            'inicio' => 'Inicio',
            'fim' => 'Fim',
            'data' => 'Data',
            'ativo' => 'Ativo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSala()
    {
        return $this->hasOne(Sala::className(), ['id' => 'sala_id']);
    }
    
    public function getHoraIniciofim()
    {
        $inicio = date('H:i', strtotime($this->inicio));
        $fim = date('H:i', strtotime($this->fim));
        return $inicio.'-'.$fim;
    }
    
    /**
     * Retorna o status do horário da sala.
     * 
     * @return string
     */
    public function getStatus()
    {
        $status = 'Disponível';
        $reserva_id = $this->id;
        $sala_id = $this->sala_id;
        $usuario_id = Yii::$app->user->identity->id;
        
        $reserva = Reserva::find()
            ->where(['sala_horario_id' => $reserva_id])
            ->andWhere(['sala_id' => $sala_id])
            ->one();
        if ($reserva) {
            if ($reserva->usuario_id == $usuario_id) {
                $status = 'Agendado';
            } else {
                $status = 'Bloqueado';
            }
        }
        
        return $status;
    }
    
    /**
     * Retorna o ID de da reserva.
     * 
     * @return integer
     */
    public function getUsuarioReservaId()
    {
        $reserva_id = $this->id;
        $sala_id = $this->sala_id;
        $usuario_id = Yii::$app->user->identity->id;
        $reserva = Reserva::find()
            ->innerJoin('sala_horario', 'sala_horario.id = reserva.sala_horario_id')
            ->where(['sala_horario_id' => $reserva_id])
            ->andWhere(['reserva.sala_id' => $sala_id])
            ->andWhere(['usuario_id' => $usuario_id])
            ->andWhere(['data' => date('Y-m-d')])
            ->one();
        return $reserva->id;
    }
    
    /**
     * Retorna o nome do usuário que fez o agendamento da sala.
     * 
     * @return string
     */
    public function getAgendadorNome()
    {
        $nome = '';
        $reserva_id = $this->id;
        $sala_id = $this->sala_id;
        $reserva = Reserva::find()
            ->innerJoin('sala_horario', 'sala_horario.id = reserva.sala_horario_id')
            ->where(['sala_horario_id' => $reserva_id])
            ->andWhere(['reserva.sala_id' => $sala_id])
            ->andWhere(['data' => date('Y-m-d')])
            ->one();
        if ($reserva) {
            $nome = $reserva->usuario->nome;
        }
        
        return $nome;
    }
}
