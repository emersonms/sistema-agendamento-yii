<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class UsuarioSearch extends Usuario
{
    public function rules()
    {
        return [
            [['perfil_id', 'ativo'], 'integer'],
            [['data_create'], 'safe'],
        ];
    }
    
    public function search($params)
    {
        $query = Usuario::find();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id',
                    'nome',
                    'username',
                    'data_create',
                ],'defaultOrder' => ['id' => SORT_ASC]
            ],
        ]);
        
        $this->load($params);
        
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }
        
        $query->andWhere(['ativo' => '1']);
        
        return $dataProvider;
    }
}
