<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sala".
 *
 * @property string $id
 * @property string $nome
 * @property integer $ativo
 *
 * @property Fila[] $filas
 */
class Sala extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sala';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome'], 'required', 'message' => '{attribute} da sala é obrigatório'],
            [['ativo'], 'integer'],
            [['data_create'], 'safe'],
            [['nome'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'data_create' => 'Data de Cadastro',
            'ativo' => 'Ativo',
        ];
    }
    
    /**
     * Verifica se a sala já possuí os horários do dia, se não possui
     * cria uma lista de horarios e retorna essa lista, se possui
     * uma lista, retorna ela.
     * 
     * @return object
     */
    public function getListaHorariosDia()
    {
        $data_atual = date('Y-m-d');
        $horarios = SalaHorario::find()
            ->where(['sala_id' => $this->id])
            ->andWhere(['data' => $data_atual])
            ->all();
        if (!$horarios) {
            for ($i=0;$i<=10;$i++) {
                $horario = new SalaHorario();
                $horario->sala_id = $this->id;
                $inicio = $data_atual.' '.str_pad((8+$i), 2, '0', STR_PAD_LEFT).':00:00';
                $fim = $data_atual.' '.str_pad((9+$i), 2, '0', STR_PAD_LEFT).':00:00';
                $horario->inicio = $inicio;
                $horario->fim = $fim;
                $horario->data = $data_atual;
                $horario->save();
            }
            $horarios = SalaHorario::find()
                ->where(['sala_id' => $this->id])
                ->andWhere(['data' => $data_atual])
                ->all();
        }
        return $horarios;
    }
}
