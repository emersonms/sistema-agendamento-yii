<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SalaSearch extends Sala
{
    public function rules()
    {
        return [
            [['nome', 'data_create', 'ativo'], 'safe'],
        ];
    }
    
    public function search($params)
    {
        $query = Sala::find();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id',
                    'nome',
                    'data_create',
                ],'defaultOrder' => ['id' => SORT_ASC]
            ],
        ]);
        
        $this->load($params);
        
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }
        
        $query->andWhere(['ativo' => '1']);
        
        return $dataProvider;
    }
}