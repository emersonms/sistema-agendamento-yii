Dados de acesso:
username: admin
senha: admin

Sistema de Agendamento
- O sistema de agendamento foi desenvolvido em PHP 7.1.11, MySQL 5.7 e Framework Yii2.

- Para realizar o acesso siga os seguintes passos:
  1) Faça o clone da url informada para o servidor local.
  2) Através do terminar acessa a pasta raiz do projeto e execute o comando 'composer install' para que seja instalado suas dependências.
  3) Efetue a instalação do dump do banco para o banco local. O nome do banco é 'sistema_agendamento' e o usuário e senha são 'root' ambos.
  4) Caso seja necessário utilizar outros dados de acesso de banco e outro nome de banco, faça as alterações no arquivo que fica em 'config/db.php'.
  4) Após as instalações concluídas acesse o projeto em http://localhost/'nome-do-projeto'/web.

- A primeira tela do sistema de agendamento é uma tela de login, seu acesso é feito utilizando o username e senha informados acima.

- Após efetuar o acesso o usuário logado é o usuário Admin. Além de possuir acesso a página principal como os demais o mesmo possui outros dois acessos, o 'gerenciar salas' e 'gerenciar usuários'.

- Gerenciar salas é utilizado para ver as salas disponíveis em forma de um gridview(lista), além da possibilidade de criação, alteração e exclusão das salas.

- Gerenciar usuários é utilizado para ver os usuários disponíveis, sendo listado com um gridview, além da possibilidade de criação, alteração e exclusão de usuários.

- Na tela principal é possível ver as salas disponíveis e seus respectivos horários, além de clicar no botão 'Agendar' para agendar uma sala ou cancelar um agendamento.

- Os status dos horários são:
1) Disponível - A sala encontra-se disponível havendo possibilidade de fazer o agendamento do mesmo através do botão 'Agendar'.
2) Agendado - Você fez o agendamento de um determinado horário de uma sala, você tem a possibilidade de cancelar esse agendamento, clicando no botão 'Cancelar'.
3) Bloqueado - Alguém fez o agendamento de um determinado horário, o mesmo encontra-se bloqueado para que os demais usuários não consigam fazer o agendamento desse horário.