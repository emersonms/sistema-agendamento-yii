<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Usuario;
use app\models\UsuarioSearch;
use yii\filters\VerbFilter;

class UsuarioController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['listar', 'criar', 'alterar', 'deletar'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['listar', 'criar', 'alterar', 'deletar'],
                        'matchCallback' => function ($rule, $action) {
                            return ((!Yii::$app->user->isGuest) && (Yii::$app->user->identity->perfil_id == 1));
                        }
                    ],
                ],
            ],
        ];
    }
    
    public function actionListar()
    {
        $searchModel = new UsuarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('listar', [
            'dataProvider' => $dataProvider,
        ]);
        
    }
    
    public function actionCriar()
    {
        $model = new Usuario();
        if ($model->load(Yii::$app->request->post())) {
            if (empty($model->password)) {
                Yii::$app->session->setFlash('error', 'A senha é obrigatório.');
            } else {
                $model->password = md5($model->password);
                $model->authkey = md5($model->username.''.time());
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'O usuário foi criado com sucesso.');
                    return $this->redirect(['listar']);
                }
            }
        }
        return $this->render('criar', [
            'model' => $model
        ]);
    }
    
    public function actionAlterar($id)
    {
        $model = Usuario::findOne(['id' => $id]);
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $nome = $post["Usuario"]["nome"];
            $username = $post["Usuario"]["username"];
            $password = $post["Usuario"]["password"];
            $perfil_id = $post["Usuario"]["perfil_id"];
            
            $model->nome = $nome;
            $model->username = $username;
            $model->perfil_id = $perfil_id;

            if (!empty($password)) {
                $model->password = md5($password);
            }
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'O usuário foi atualizado com sucesso.');
                return $this->redirect(['listar']);
            }
        }
        $model->password = '';
        return $this->render('alterar', [
            'model' => $model
        ]);
    }
    
    public function actionDeletar($id)
    {
        $usuario = Usuario::findOne(['id' => $id]);
        if ($usuario) {
            $usuario->ativo = 0;
            if ($usuario->save()) {
                Yii::$app->session->setFlash('success', 'O usuário foi deletado com sucesso.');
            }
        }
        return $this->redirect(['listar']);
    }
}
