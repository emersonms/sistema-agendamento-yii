<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Reserva;
use app\models\SalaHorario;
use app\models\SalaSearch;
use app\models\Sala;
use yii\filters\VerbFilter;

class SalaController extends Controller
{
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['listar', 'alterar', 'deletar', 'criar', 'agendar', 'cancelar'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['agendar', 'cancelar'],
                        'matchCallback' => function ($rule, $action) {
                            return (!Yii::$app->user->isGuest);
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['listar', 'criar', 'alterar', 'deletar'],
                        'matchCallback' => function ($rule, $action) {
                            return ((!Yii::$app->user->isGuest) && (Yii::$app->user->identity->perfil_id == 1));
                        }
                    ],
                ],
            ],
        ];
    }
    
    public function actionListar()
    {
        $searchModel = new SalaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('listar', [
            'dataProvider' => $dataProvider,
        ]);
        
    }
    
    public function actionCriar()
    {
        $model = new Sala();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'A sala foi criada com sucesso.');
                return $this->redirect(['listar']);
            }
        }
        return $this->render('criar', [
            'model' => $model
        ]);
    }
    
    public function actionAlterar($id)
    {
        $model = Sala::findOne(['id' => $id]);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'O nome da sala foi alterada com sucesso.');
                return $this->redirect(['listar']);
            }
        }
        return $this->render('alterar', [
            'model' => $model
        ]);
    }
    
    public function actionDeletar($id)
    {
        $sala = Sala::findOne(['id' => $id]);
        if ($sala) {
            $sala->ativo = 0;
            if ($sala->save()) {
                Yii::$app->session->setFlash('success', 'A sala foi deletada com sucesso.');
            }
        }
        return $this->redirect(['listar']);
    }

    public function actionAgendar($id)
    {
        $usuario_id = Yii::$app->user->identity->id;
        $sala_horario = SalaHorario::findOne(['id' => $id]);
        if ($sala_horario) {
            $reserva = new Reserva();
            $reserva->sala_id = $sala_horario->sala_id;
            $reserva->sala_horario_id = $id;
            $reserva->usuario_id = $usuario_id;
            if ($reserva->save()) {
                Yii::$app->session->setFlash('success', 'O horário da sala foi reservado com sucesso.');
            }
        }
        return $this->redirect(['site/index']);
    }
    
    public function actionCancelar($id)
    {
        $usuario_id = Yii::$app->user->identity->id;
        $reserva = Reserva::findOne(['id' => $id]);
        if ($reserva) {
            $reserva->delete();
            Yii::$app->session->setFlash('success', 'O horário reservado da sala foi cancelado com sucesso.');
        }
        return $this->redirect(['site/index']);
    }
}
